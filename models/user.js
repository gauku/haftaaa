var mongoose = require('mongoose');
var Schema = mongoose.Schema;

mongoose.connect('mongodb://huserdbadmin:huserdbpwdadmin:27017@localhost:/huserdb');

var userSchema = new Schema({
	email: {
		type: String, 
   		required: true, 
   		unique: true
   	},
   	myCode: { type: String },
    referredBy: { type: String },
    salaryGateway: { type: Number },
    referredUsers: { type: Array },
    wallethistory: { type: Array }
    // created_at: Date,
    // updated_at: Date
});

var User = mongoose.model('User', userSchema);

module.exports = User;