var hapi = require('hapi');
var server = new hapi.Server();
var cc = require('coupon-code');

server.connection({
        host: 'ec2-52-66-41-162.ap-south-1.compute.amazonaws.com',
        port: 8080
});


var home = function(req, reply) {
        reply.file('./public/index.html');
      };

var signup = function(req, reply) {

        var User = require('./models/user');
        var myrefCode = cc.generate({ parts : 1, partLen : 6 });
        var replystring;

        User.find({email: req.payload.email}, function(err, user) {
          if(user[0])
          {
             replystring = {
              "data": {
                "id" : user[0]._id
              },
              "status": "failure",
               "message": "User already exists!"
               };
           reply(replystring);
          }
          else
          {
            var newUser = new User({
              email: req.payload.email,
              myCode: myrefCode,
            });

            newUser.save(function(err) {
              if (err)
                reply({"status": "failure", "message": "Error saving in database"});
            });

            if(req.payload.referralcode)
            {
              User.find({myCode: req.payload.referralcode}, function(err, user) {
                if (err) 
                {
                }
                else
                {
                  newUser.referredBy = user[0]._id;
                  user[0].referredUsers.push(newUser._id);

                  newUser.save(function(err) {
                    if (err)
                      reply({"status": "failure", "message": "Error saving in database"});
                 
                    console.log('newUser referred by: ' + newUser.referredBy);
                  });
                  user[0].save(function(err) {
                    if (err)
                      reply({"status": "failure", "message": "Error saving in database"});
                      
                    console.log('Referred users: ' + user[0].referredUsers);
                  });
                }
              });
            }
            console.log(newUser);
            replystring = {
              "data": {
                "id" : newUser._id
              },
              "status": "success",
               "message": "User registered successfully."
               };
            reply(replystring);
          }
        });
      }

var setupgateway = function(req, reply) {

        if(!req.payload.id || !req.payload.salaryGateway)
          return reply({"status": "failure", "message": "No ID or Salary Gateway received"});

        var User = require('./models/user');
        
        User.find({_id: req.payload.id}, function(err, user) {
          if (err)
            reply({"status": "failure", "message": "Error finding the user in database"});
          
        
          user[0].salaryGateway = req.payload.salaryGateway;
        
          user[0].save(function(err) {
            if (err)
              reply({"status": "failure", "message": "Error saving in database"});
            
            console.log('salary Gateway for ' + user[0].email + ' is ' + user[0].salaryGateway);
          });
        });
        reply({"status": "success", "message": "Salary Gateway setup successful"});
      }

var currentgateway = function(req, reply) {

        if(!req.payload.id)
          return reply({"status": "failure", "message": "No ID received"});

        var User = require('./models/user');
        var replystring;
  
        User.find({_id: req.payload.id}, function(err, user) {
          if (err)
           throw err;
  
          if(user[0].salaryGateway)
            replystring = {
              "data": {
                "salaryGateway" : user[0].salaryGateway
              },
              "status": "success",
               "message": "Salary Gateway found"
             };        
          else
            replystring = {
              "status": "failure",
              "message": "No Salary Gateway found"
             };
          reply(replystring);
      });
      }

var myreferrralcode = function(req, reply) {

        if(!req.payload.id)
          return reply({"status": "failure", "message": "No ID received"});

        var User = require('./models/user');
        var replystring;

        User.find({_id: req.payload.id}, function(err, user) {
          if (err)
           throw err;
          console.log(user[0]);
            replystring = {
              "data": {
                "referralcode" : user[0].myCode
              },
              "status": "success",
               "message": "Referral Code retrieved successfully"
             };        
          reply(replystring);
        });
      }

var updatewallethistory = function(req, reply) {
 
        if(!req.payload.id || !req.payload.currentwalletbalance)
          return reply({"status": "failure", "message": "No ID or Current Wallet Balance received"});
 
        var User = require('./models/user');
 
        User.find({_id: req.payload.id}, function(err, user) {
          if (err)
            return reply({"status": "failure", "message": "Error saving in database"});
 
          var month = new Date().getMonth();
          var balance;
          if(month == 0)
            balance = {"January" : req.payload.currentwalletbalance};
          else if(month == 1)
            balance = {"February" : req.payload.currentwalletbalance};
          else if(month == 2)
            balance = {"March" : req.payload.currentwalletbalance};
          else if(month == 3)
            balance = {"April" : req.payload.currentwalletbalance};
          else if(month == 4)
            balance = {"May" : req.payload.currentwalletbalance};
          else if(month == 5)
            balance = {"June" : req.payload.currentwalletbalance};
          else if(month == 6)
            balance = {"July" : req.payload.currentwalletbalance};
          else if(month == 7)
            balance = {"August" : req.payload.currentwalletbalance};
          else if(month == 8)
            balance = {"September" : req.payload.currentwalletbalance};
          else if(month == 9)
            balance = {"October" : req.payload.currentwalletbalance};
          else if(month == 10)
            balance = {"November" : req.payload.currentwalletbalance};
          else if(month == 11)
            balance = {"December" : req.payload.currentwalletbalance};
          
          var balance_string = JSON.stringify(balance);
          user[0].wallethistory.push(balance_string);
 
          user[0].save(function(err) {
            if (err)
              return reply({"status": "failure", "message": "Error saving in database"});
            console.log('Wallet Balance for ' + user[0].email + ' is ' + user[0].wallethistory);
          });
          reply({"status": "success", "message": "Wallet History update successful"});
        });
      }

var getwallethistory = function(req, reply) {
        if(!req.payload.id)
          return reply({"status": "failure", "message": "No ID received"});
  
        var User = require('./models/user');
  
        User.find({_id: req.payload.id}, function(err, user) {
          if (err) 
              return reply({"status": "failure", "message": "Error finding in database"});

            var replystring = {
              "data": {
                "wallethistory" : user[0].wallethistory
              },
              "status": "success",
               "message": "Wallet History retrieved successfully"
             };
             reply(replystring);
        });
      }

server.register([{ 
	register: require('inert')}], function(err) { 
	if(err)
		return console.log(err);

	//register the routes
	server.route([
        { method: 'GET', path: '/', config: { handler: home} },
        { path: '/signup', method: 'POST', config: { handler: signup} },
        { path: '/setupgateway', method: 'POST', config: { handler: setupgateway} },
        { path: '/currentgateway', method: 'POST', config: { handler: currentgateway} },
        { path: '/myreferrralcode', method: 'POST', config: { handler: myreferrralcode} },
        { path: '/updatewallethistory', method: 'POST', config: { handler: updatewallethistory} },
        { path: '/getwallethistory', method: 'POST', config: { handler: getwallethistory} }
		]);

	server.start(function(){ 
		console.log('Server listening at 8080');
	});
});